package lab1.hadoop;

import org.apache.hadoop.util.ProgramDriver;

public class LogDriver {
    
	public static void main( String[] args ) {
    	ProgramDriver pgd = new ProgramDriver();
		int exitCode = -1;
		try {
			pgd.addClass("loganalyzer", LogAnalyzer.class, "Extracts Browsers from Apache log files");
			pgd.driver(args);
			exitCode = 0;
		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.exit(exitCode);
    }
}
