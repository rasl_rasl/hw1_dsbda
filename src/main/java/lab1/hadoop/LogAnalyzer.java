package lab1.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;


public class LogAnalyzer {
	public static final String MALFORMED_ROWS_COUNTER = "MALFORMED_ROWS";
	public static final int reducerNumber = 2;

	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration(); // Create configuration
		
		// Check args
		if (args.length != 2) {
			System.err.println("Usage: loganalyzer <in> <out>");
			System.exit(2);
		}
		
		@SuppressWarnings("deprecation")
		Job job = new Job(conf); // Create job
		job.setJobName("Analyze log");
		job.setJarByClass(LogAnalyzer.class);

		job.setMapperClass(LogMapper.class); // Setup Mapper
		job.setReducerClass(LogReducer.class); // Setup Rreducer
		
		job.setNumReduceTasks(reducerNumber); // Number of Reducers more than 1
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		FileInputFormat.addInputPath(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		job.setOutputFormatClass(SequenceFileOutputFormat.class); // Output Format - SequenceFile
		
		System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
