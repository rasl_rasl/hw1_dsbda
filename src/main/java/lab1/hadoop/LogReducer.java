package lab1.hadoop;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

// Reducer class
public class LogReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
	private IntWritable total = new IntWritable();

	// Reduce function
	public void reduce(Text key, Iterable<IntWritable> values, Context context)
			throws IOException, InterruptedException {
		// sums up the values, which are the occurrence counts for each key  
		int sum = 0;
		for (IntWritable value : values) {
			sum += value.get();
		}
		total.set(sum);
		context.write(key, total);
	}
}
