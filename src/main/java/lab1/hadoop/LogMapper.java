package lab1.hadoop;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

// Mapper class
public class LogMapper extends Mapper<Object, Text, Text, IntWritable> {
	private final static IntWritable addone = new IntWritable(1);
	private Text browser_name = new Text();
	
	private Pattern p = Pattern.compile("(\"[^\"]*\")$"); // pattern for detection user-agent string
	
	// Map function
	public void map(Object key, Text value, Context context) 
		throws IOException, InterruptedException {
			Matcher matcher = p.matcher(value.toString());
			String browser = null;
			if (matcher.find()) { 
				
				//browser detection in user-agent
				if (matcher.group(0).contains("Firefox") 
						&& !matcher.group(0).contains("Seamonkey")) {
					browser = "Firefox";
				}
				if (matcher.group(0).contains("Seamonkey")) {
					browser = "Seamonkey";
				}
				if (matcher.group(0).contains("Chrome") 
						&& !matcher.group(0).contains("Chromium")) {
					browser = "Chrome";
				}
				if (matcher.group(0).contains("Chromium")) {
					browser = "Chromium";
				}
				if (matcher.group(0).contains("Safari") 
						&& !matcher.group(0).contains("Chrome") 
						&& !matcher.group(0).contains("Chromium")) {
					browser = "Safari";
				}
				if (matcher.group(0).contains("OPR") 
						|| matcher.group(0).contains("Opera")) {
					browser = "Opera";
				}
				if (matcher.group(0).contains("MSIE")) {
					browser = "MSIE";
				}
			}
			
			if (browser == null) {
				// Counters is used for statistics about malformed rows collection
				context.getCounter(LogAnalyzer.MALFORMED_ROWS_COUNTER, LogAnalyzer.MALFORMED_ROWS_COUNTER).increment(1);
			} else {
				browser_name.set(browser);
				context.write(browser_name, addone);
			}
			
	}
}
