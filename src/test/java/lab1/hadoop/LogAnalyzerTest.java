package lab1.hadoop;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import lab1.hadoop.LogAnalyzer;
import lab1.hadoop.LogMapper;
import lab1.hadoop.LogReducer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;

import org.junit.Before;
import org.junit.Test;

public class LogAnalyzerTest {
	
	private MapDriver<Object, Text, Text, IntWritable> mapDriver;
	private ReduceDriver<Text, IntWritable, Text, IntWritable> reduceDriver;
	private MapReduceDriver<Object, Text, Text, IntWritable, Text, IntWritable> mapReduceDriver;
	
	@Before
	public void setUp(){
		LogMapper mapper = new LogMapper();
		LogReducer reducer = new LogReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
		
	}
	
	@Test
	public void testMapper(){
		mapDriver.withInput(new Object(), new Text("81.73.150.239 - - [10/Oct/2013:00:02:49 ] \"GET /shifters HTTP/1.0\" 200 3122 \"http://bestcyclingreviews.com/top_online_shops\" \"Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))\""));
		mapDriver.withOutput(new Text("MSIE"), new IntWritable(1));
		// should be MSIE - 1
		mapDriver.runTest();
	}
	
	@Test
	public void testReducer() {
	    List<IntWritable> values = new ArrayList<IntWritable>();
	    values.add(new IntWritable(1));
	    values.add(new IntWritable(1));
	    reduceDriver.withInput(new Text("MSIE"), values);
	    reduceDriver.withOutput(new Text("MSIE"), new IntWritable(2));
	    // should be MSIE - 2
	    reduceDriver.runTest();
	}
	
	@Test
	public void testMapReduce() {
	    mapReduceDriver.addInput(new Object(), new Text("81.73.150.239 - - [10/Oct/2013:00:02:49 ] \"GET /shifters HTTP/1.0\" 200 3122 \"http://bestcyclingreviews.com/top_online_shops\" \"Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))\""));
	    mapReduceDriver.addInput(new Object(), new Text("10.182.189.79 - - [10/Oct/2013:00:21:44 ] \"GET /shifters HTTP/1.0\" 200 3929 \"http://www.casualcyclist.com\" \"Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)\""));
	    mapReduceDriver.withOutput(new Text("MSIE"), new IntWritable(2));
	    // should be MSIE - 2
	    mapReduceDriver.runTest();
	}
	
	@Test
	public void testCounterMalformedRowsPositive(){
		mapDriver.withInput(new Object(), new Text("81.73.150.239 - - [10/Oct/2013:00:02:49 ] \"GET /shifters HTTP/1.0\" 200 3122 \"http://bestcyclingreviews.com/top_online_shops\" \"Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US))\""));
		mapDriver.withOutput(new Text("MSIE"), new IntWritable(1));
		mapDriver.runTest();
		// malformed rows should be 0
		assertEquals(0, mapDriver.getCounters().findCounter(LogAnalyzer.MALFORMED_ROWS_COUNTER, LogAnalyzer.MALFORMED_ROWS_COUNTER).getValue());
	}
	
	@Test
	public void testCounterMalformedRowsNegative(){
		mapDriver.withInput(new Object(), new Text("81.73.150.239 - - [10/Oct/2013:00:02:49 ] \"GET /shifters HTTP/1.0\" 200 3122 \"http://bestcyclingreviews.com/top_online_shops\""));
		mapDriver.runTest();
		// malformed rows should be 1
		assertEquals(1, mapDriver.getCounters().findCounter(LogAnalyzer.MALFORMED_ROWS_COUNTER, LogAnalyzer.MALFORMED_ROWS_COUNTER).getValue());
	}
}