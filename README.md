# How to set up #

## 1. Clone the project ##
git clone https://rasl_rasl@bitbucket.org/rasl_rasl/hw1_dsbda.git

## 2. Move to the directory of the project ##
cd hw1_dsbda

## 3. Run script to make access.log ##
gen_log.py

## 4. Build project ##
mvn package

# How to run #

## 1. Put access.log into HDFS ##
hdfs dfs -put access.log

## 2. Run mapreduce job ##
hadoop jar target/hadoop_lab_1-1.0.jar lab1.hadoop.LogAnalyzer access.log output
